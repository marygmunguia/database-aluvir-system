-- -----------------------------------------------------
-- Schema AS
-- -----------------------------------------------------
-- BASE DE DATOS DE ALUVIR SYSTEM
DROP SCHEMA IF EXISTS `AS` ;

-- -----------------------------------------------------
-- Schema AS
--
-- BASE DE DATOS DE ALUVIR SYSTEM
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `AS` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish2_ci ;
USE `AS` ;

-- -----------------------------------------------------
-- Table `AS`.`AS_EMPLEADOS`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AS`.`AS_EMPLEADOS` ;

CREATE TABLE IF NOT EXISTS `AS`.`AS_EMPLEADOS` (
  `PK_COD_EMPLEADO` INT NOT NULL AUTO_INCREMENT COMMENT 'CODIGO ÚNICO DE CADA REGISTO EN LA TABLA [ABR: CODEMPL]',
  `NOMBRE` VARCHAR(45) NOT NULL COMMENT 'PRIMER Y/O SEGUNDO NOMBRE DEL EMPLEADO',
  `APELLIDO` VARCHAR(45) NOT NULL COMMENT 'PRIMER Y/O SEGUNDO APELLIDO DEL EMPLEADO',
  `FEC_NACIMIENTO` DATE NULL COMMENT 'FECHA DE NACIMIENTO DEL EMPLEADO',
  `SEXO` VARCHAR(1) NOT NULL COMMENT 'SEXO DEL EMPLEADO',
  `DIRECCION` TEXT NULL COMMENT 'DOMICILIO ACTUAL DEL EMPLEADO',
  `TELEFONO` VARCHAR(20) NULL COMMENT 'NÚMERO DE TELEFONO FIJO DEL EMPLEADO',
  `CELULAR` VARCHAR(20) NOT NULL COMMENT 'NÚMERO DE TELEFONO MÓVIL DEL EMPLEADO',
  `EMAIL` VARCHAR(50) NOT NULL COMMENT 'CORREO ELECTRONICO DEL EMPLEADO',
  PRIMARY KEY (`PK_COD_EMPLEADO`))
COMMENT 'TABLA CON LOS DATOS DE TODOS LOS EMPLEADOS DE LA EMPRESA [ABR: EMPL]',
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AS`.`AS_USUARIOS_SISTEMA`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AS`.`AS_USUARIOS_SISTEMA` ;

CREATE TABLE IF NOT EXISTS `AS`.`AS_USUARIOS_SISTEMA` (
  `PK_ID_USUARIO` INT NOT NULL AUTO_INCREMENT COMMENT 'IDENTIFICADOR UNICO DE LA TABLA [ABR:IDUSUSTM]',
  `NOM_USUARIO` VARCHAR(30) NOT NULL COMMENT 'NOMBRE DE USUARIO EN EL SISTEMA',
  `PASS_USUARIO` VARCHAR(100) NOT NULL COMMENT 'CONTRASEÑA CIFRADA DE ACCESO AL SISTEMA',
  `IMG_USUARIO` VARCHAR(80) NULL COMMENT 'IMAGEN DEL USUARIO EN EL SISTEMA',
  `EST_USUARIO` VARCHAR(10) NOT NULL COMMENT 'ESTADO DEL USUARIO EN EL SISTEMA (ACTIVO E INACTIVO)',
  `ID_EMPLEADO` INT NOT NULL COMMENT 'IDENTIFICADOR UNICO DE LOS DATOS DEL USUARIO EN SISTEMA',
  PRIMARY KEY (`PK_ID_USUARIO`),
  INDEX `IDX_USUARIOSSTM_1` (`ID_EMPLEADO` ASC),
  CONSTRAINT `FK_USUARIOSSTM_EMPL`
    FOREIGN KEY (`ID_EMPLEADO`)
    REFERENCES `AS`.`AS_EMPLEADOS` (`PK_COD_EMPLEADO`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
COMMENT 'TABLA CON LOS DATOS DE TODOS LOS USUARIO REGISTRADOS EN EL SISTEMA [ABR:USUARIOSSTM]',
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AS`.`AS_BITACORA_SISTEMA`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AS`.`AS_BITACORA_SISTEMA` ;

CREATE TABLE IF NOT EXISTS `AS`.`AS_BITACORA_SISTEMA` (
  `PK_COD_BITACORA` INT NOT NULL AUTO_INCREMENT COMMENT 'CODIGO DE LA ACCION REALIZADA EN EL SISTEMA [ABR:CODBISTM]',
  `ID_USUARIO` INT NOT NULL COMMENT 'IDENTIFICADOR DEL USUARIO QUE REALIZO LA ACCIÓN',
  `NOM_ACCION` VARCHAR(150) NOT NULL COMMENT 'NOMBRE BREVE DE LA ACCIÓN REALIZADA',
  `DET_BITACORA_SISTEMA` TEXT NULL COMMENT 'DESCRIPCIÓN COMPLETA DE LA ACCIÓN REALIZADA',
  `FEC_ACCION` DATE NOT NULL COMMENT 'FECHA EN QUE SE REALIZO DICHA ACCIÓN',
  `HORA_ACCION` TIME NOT NULL COMMENT 'HORA EN QUE SE REALIZO DICHA ACCIÓN',
  PRIMARY KEY (`PK_COD_BITACORA`),
  INDEX `IDX_BITCRSISTEM_1` (`ID_USUARIO` ASC),
  CONSTRAINT `FK_BITCRSISTEM_USUARIOSSTM`
    FOREIGN KEY (`ID_USUARIO`)
    REFERENCES `AS`.`AS_USUARIOS_SISTEMA` (`PK_ID_USUARIO`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
COMMENT 'TABLA CON LOS REGISTROS DE TODAS LAS ACCIONES REALIZADAS POR LOS USUARIOS EN EL SISTEMA [ABR:BITCRSISTEM]',
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AS`.`AS_ACCESO_SISTEMA`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AS`.`AS_ACCESO_SISTEMA` ;

CREATE TABLE IF NOT EXISTS `AS`.`AS_ACCESO_SISTEMA` (
  `PK_COD_ACCESO` INT NOT NULL AUTO_INCREMENT COMMENT 'CODIGO UNICO DE CADA ACCESO [ABR:CODACSSTM]',
  `NOM_ACCESO` VARCHAR(75) NOT NULL COMMENT 'NOMBRE DESCRIPTIVO DE CADA ACCESO',
  `DET_ACCESO` TEXT NULL COMMENT 'DETALLES DE LAS CAPACIDADES DEL ACCESO',
  PRIMARY KEY (`PK_COD_ACCESO`))
COMMENT 'TABLA CON LOS REGISTROS DE TODAS LAS ACCIONES REALIZADAS POR LOS USUARIOS EN EL SISTEMA [ABR:ACCESSISTEM]',
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AS`.`AS_ACCESO_PERMISO_USUARIO`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AS`.`AS_ACCESO_PERMISO_USUARIO` ;

CREATE TABLE IF NOT EXISTS `AS`.`AS_ACCESO_PERMISO_USUARIO` (
  `PK_COD_PERMISO` INT NOT NULL AUTO_INCREMENT COMMENT 'CODIGO IDENTIFICADOR ÚNICO DE LA TABLA [ABR:CODAPUSU]',
  `ID_USUARIO` INT NOT NULL COMMENT 'IDENTIFIADOR DEL USUARIO AL QUE PERTENECE EL PERMISO',
  `COD_ACCESO_SISTEMA` INT NOT NULL COMMENT 'CODIGO IDENTIFICADOR ÚNICO DE ACCESO AL SISTEMA',
  PRIMARY KEY (`PK_COD_PERMISO`),
  INDEX `IDX_ACSPERMSUSU_1` (`COD_ACCESO_SISTEMA` ASC),
  INDEX `IDX_ACSPERMSUSU_2` (`ID_USUARIO` ASC),
  CONSTRAINT `FK_ACSPERMSUSU_ACCESSISTEM`
    FOREIGN KEY (`COD_ACCESO_SISTEMA`)
    REFERENCES `AS`.`AS_ACCESO_SISTEMA` (`PK_COD_ACCESO`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_ACSPERMSUSU_USUARIOSSTM`
    FOREIGN KEY (`ID_USUARIO`)
    REFERENCES `AS`.`AS_USUARIOS_SISTEMA` (`PK_ID_USUARIO`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
COMMENT 'TABLA CON EL REGISTRO DE TODOS LOS PERMISOS OTORGADOS A LOS USUARIOS [ABR:ACSPERMSUSU]',
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AS`.`AS_TIPOS_IDENTIFICACIONES`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AS`.`AS_TIPOS_IDENTIFICACIONES` ;

CREATE TABLE IF NOT EXISTS `AS`.`AS_TIPOS_IDENTIFICACIONES` (
  `PK_COD_TIPO_IDENTIFICACION` INT NOT NULL AUTO_INCREMENT COMMENT 'CODIGO IDENTIFICADOR ÚNICO DE LA TABLA [ABR:CODTIPID]',
  `NOM_TIPO_IDENTIFICACION` VARCHAR(45) NOT NULL COMMENT 'NOMBRE CORTO DESCRIPTIVO DE CADA TIPO DE IDENTIFICACION',
  `DET_TIPO_IDENTIFICACION` TEXT NULL COMMENT 'DETALLE COMPLETO DE CADA TIPO DE IDENTIFICACION',
  PRIMARY KEY (`PK_COD_TIPO_IDENTIFICACION`))
COMMENT 'TABLA CON LOS REGISTROS DE LOS TIPOS DE IDENTIFICACIONES VALIDAS EN EL PAIS [ABR:TIPOSDEIDNTS]',
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AS`.`AS_CLIENTES`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AS`.`AS_CLIENTES` ;

CREATE TABLE IF NOT EXISTS `AS`.`AS_CLIENTES` (
  `PK_COD_CLIENTE` INT NOT NULL AUTO_INCREMENT COMMENT 'CODIGO IDENTIFICADOR ÚNICO DE LA TABLA [ABR: CODCLN]',
  `COD_TIPO_IDENTIFICACION` INT NULL COMMENT 'CODIGO DEL TIPO DE IDENTIFICACION CON EL QUE SE ESTA REGISTRANDO EL CLIENTE',
  `NO_IDENTIFICACION` VARCHAR(20) NULL COMMENT 'NUMERO DE IDENTIFICACION DEL CLIENTE',
  `NOM_CLIENTE` VARCHAR(45) NOT NULL COMMENT 'PRIMER Y/O SEGUNDO NOMBRE DEL CLIENTE',
  `APE_CLIENTE` VARCHAR(45) NOT NULL  COMMENT 'PRIMER Y/O SEGUNDO APELLIDO DEL CLIENTE',
  `EMAIL` VARCHAR(50) NULL  COMMENT 'CORREO ELECTRONICO DEL CLIENTE',
  `TELEFONO` VARCHAR(20) NULL COMMENT 'NUMERO DE TELEFONO FIJO DEL CLIENTE',
  `CELULAR` VARCHAR(20) NOT NULL COMMENT 'NUMERO DE TELEFONO MOVIL DEL CLIENTE',
  `CREDITO` INT(1) NOT NULL COMMENT 'DISPONIBILIDAD DE CREDITO DEL CLIENTE',
  `LIM_CREDITO` DECIMAL(7,2) NOT NULL COMMENT 'LIMITE DE CREDITO QUE SE LE PUEDE DAR AL CLIENTE',
  PRIMARY KEY (`PK_COD_CLIENTE`),
  INDEX `IDX_CLIENTES_1` (`COD_TIPO_IDENTIFICACION` ASC),
  CONSTRAINT `FK_CLIENTES_TIPOSDEIDNTS`
    FOREIGN KEY (`COD_TIPO_IDENTIFICACION`)
    REFERENCES `AS`.`AS_TIPOS_IDENTIFICACIONES` (`PK_COD_TIPO_IDENTIFICACION`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
COMMENT 'TABLA QUE CONTIENE TODOS LOS DATOS DE LOS CLIENTES REGISTRADOS [ABR:CLIENTES]',
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AS`.`AS_SEGUIMIENTO_CLIENTES`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AS`.`AS_SEGUIMIENTO_CLIENTES` ;

CREATE TABLE IF NOT EXISTS `AS`.`AS_SEGUIMIENTO_CLIENTES` (
  `PK_COD_SEGUIMIENTO` INT NOT NULL AUTO_INCREMENT COMMENT 'CODIGO ÚNICO DEL CASO DE SEGUIMIENTO Y SERVICIO AL CLIENTE [ABR: CODSEGUI]',
  `ID_VENTA` INT NOT NULL COMMENT 'IDENTIFICADOR DE LA VENTA A LA QUE SE LE ESTA DANDO SEGUIMIENTO',
  `ID_CLIENTE` INT NOT NULL COMMENT 'IDENTIFICADOR DEL CLIENTE QUE REALIZO LA COMPRA',
  `FEC_SEGUIMIENTO` DATE NOT NULL COMMENT 'FECHA QUE SE REGISTRA EL SEGUIMIENTO',
  `DET_RESEÑA` TEXT NULL COMMENT 'DETALLE DE LA RESEÑA O CRITICA DEL CLIENTE',
  `NUM_CALIFICACION` INT(2) NOT NULL COMMENT 'NUMERO DE LA CALIFICACION DEL 1 A 10 DADA POR EL CLIENTE',
  PRIMARY KEY (`PK_COD_SEGUIMIENTO`),
  INDEX `IDX_SEGUICLIENT_1` (`ID_CLIENTE` ASC),
  CONSTRAINT `FK_SEGUICLIENT_CLIENTES`
    FOREIGN KEY (`ID_CLIENTE`)
    REFERENCES `AS`.`AS_CLIENTES` (`PK_COD_CLIENTE`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
COMMENT 'TABLA CON EL REGISTRO DE LAS CRITICAS DE TODOS LOS CLIENTES [ABR:SEGUICLIENT]',
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AS`.`AS_DIRECCIONES_CLIENTES`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AS`.`AS_DIRECCIONES_CLIENTES` ;

CREATE TABLE IF NOT EXISTS `AS`.`AS_DIRECCIONES_CLIENTES` (
  `PK_COD_DIRECCION` INT NOT NULL AUTO_INCREMENT COMMENT 'CODIGO IDENTIFICADOR ÚNICO DE LA TABLA [ABR: CODDIR]',
  `COD_CLIENTE` INT NOT NULL COMMENT 'CODIGO UNICO DE CADA CLIENTE REGISTARADO',
  `DEPARTAMENTO` VARCHAR(45) NOT NULL COMMENT 'NOMBRE DEL DEPARTAMENTO',
  `MUNICIPIO` VARCHAR(45) NOT NULL COMMENT 'NOMBRE DEL MUNICIPIO',
  `CIUDAD` VARCHAR(45) NOT NULL COMMENT 'NOMBRE DE LA CIUDAD',
  `BARRIO_COLONIA` VARCHAR(45) NOT NULL COMMENT 'NOMBRE DEL BARRIO Y/O COLONIA',
  `DET_DIRECCION` TEXT NOT NULL COMMENT 'DETALLE COMPLETO DE LA DIRECCION DEL CLIENTE',
  PRIMARY KEY (`PK_COD_DIRECCION`),
  INDEX `IDX_DIRCLN_1` (`COD_CLIENTE` ASC),
  CONSTRAINT `FK_DIRCLN_CLIENTES`
    FOREIGN KEY (`COD_CLIENTE`)
    REFERENCES `AS`.`AS_CLIENTES` (`PK_COD_CLIENTE`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
COMMENT 'TABLA QUE CONTIENE TODAS LAS DIRECCIONES REGISTRADAS DE LOS CLIENTES [ABR:DIRCLN]',
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AS`.`AS_CUENTAS_CLIENTES`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AS`.`AS_CUENTAS_CLIENTES` ;

CREATE TABLE IF NOT EXISTS `AS`.`AS_CUENTAS_CLIENTES` (
  `PK_NO_CUENTA` INT NOT NULL AUTO_INCREMENT COMMENT 'NUMERO UNICO DE CADA CUENTA',
  `COD_CLIENTE` INT NOT NULL COMMENT 'CODIGO UNICO DE CADA CLIENTE',
  `SALDO` DECIMAL(7,2) NOT NULL COMMENT 'SALDO DAUDOR DE LA CUENTA',
  PRIMARY KEY (`PK_NO_CUENTA`),
  INDEX `IDX_CUENTAS_1` (`COD_CLIENTE` ASC),
  CONSTRAINT `FK_CUENTAS_CLIENTES`
    FOREIGN KEY (`COD_CLIENTE`)
    REFERENCES `AS`.`AS_CLIENTES` (`PK_COD_CLIENTE`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
COMMENT 'TABLA QUE CONTIENE TODAS LAS CUENTAS REGISTRADAS DE LOS CLIENTES [ABR:CUENTAS]',
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AS`.`AS_METODOS_PAGOS`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AS`.`AS_METODOS_PAGOS` ;

CREATE TABLE IF NOT EXISTS `AS`.`AS_METODOS_PAGOS` (
  `PK_COD_METODO_PAGO` INT NOT NULL AUTO_INCREMENT COMMENT 'CODIGO UNICO DE CADA MEDOTO DE PAGO',
  `NOM_METODO_PAGO` VARCHAR(45) NOT NULL COMMENT 'NOMBRE CORTO DEL METODO DE PAGO',
  `DET_METODO_PAGO` TEXT NULL COMMENT 'DETALLE COMPLETO DEL METODO DE PAGO',
  PRIMARY KEY (`PK_COD_METODO_PAGO`))
COMMENT 'TABLA QUE CONTIENE TODOS LOS METODOS DE PAGO DISPONIBLES EN EL SISTEMA [ABR:METPAGOS]',
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AS`.`AS_ABONOS_CLIENTES`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AS`.`AS_ABONOS_CLIENTES` ;

CREATE TABLE IF NOT EXISTS `AS`.`AS_ABONOS_CLIENTES` (
  `PK_COD_ABONO` INT NOT NULL COMMENT 'CODIGO UNICO DE CADA ABONO REALIZADO',
  `NO_CUENTA` INT NULL COMMENT 'NUMERO UNICO DE CUENTA DE CADA CLIENTE',
  `FEC_ABONO` DATE NULL COMMENT 'FECHA ',
  `MON_ABONO` DECIMAL(7,2) NULL,
  `COD_METODO_PAGO` INT NULL,
  PRIMARY KEY (`PK_COD_ABONO`),
  INDEX `IDX_ABONOS_1` (`COD_METODO_PAGO` ASC),
  INDEX `IDX_ABONOS_2` (`NO_CUENTA` ASC),
  CONSTRAINT `FK_ABONOS_METPAGOS`
    FOREIGN KEY (`COD_METODO_PAGO`)
    REFERENCES `AS`.`AS_METODOS_PAGOS` (`PK_COD_METODO_PAGO`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_ABONOS_CUENTAS`
    FOREIGN KEY (`NO_CUENTA`)
    REFERENCES `AS`.`AS_CUENTAS_CLIENTES` (`PK_NO_CUENTA`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
COMMENT 'TABLA QUE REGISTRA CADA ABONO REALIZADO POR UN CLIENTE [ABR:ABONOS]',
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AS`.`AS_PROVEEDORES`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AS`.`AS_PROVEEDORES` ;

CREATE TABLE IF NOT EXISTS `AS`.`AS_PROVEEDORES` (
  `PK_COD_PROVEEDOR` INT NOT NULL AUTO_INCREMENT COMMENT "CODIGO UNICO DE CADA REGISTRO",
  `RTN_PROVEEDOR` VARCHAR(20) NULL COMMENT "RTN DEL PROVEEDOR",
  `NOM_PROVEEDOR` VARCHAR(45) NOT NULL COMMENT "NOMBRE DEL PROVEEDOR",
  `DIRECCION` TEXT NULL COMMENT "DIRECCION DEL PROVEEDOR",
  `EMAIL` VARCHAR(50) NULL COMMENT "EMAIL DEL PROVEEDOR",
  `TELEFONO` VARCHAR(20) NULL COMMENT "NUMERO DE TELEFONO FIJO",
  `FAX` VARCHAR(20) NULL COMMENT "NUMERO DE FAX",
  `CELULAR` VARCHAR(20) NULL COMMENT "NUMERO DE TELEFONO MOVIL",
  `SITIO_WEB` VARCHAR(45) NULL COMMENT "DIRECCION WEB",
  `ASESOR_VENTAS` VARCHAR(70) NULL COMMENT "NOMBRE COMPLETO DEL ASESOR DE VENTAS",
  `EMAIL_ASESOR_VENTAS` VARCHAR(50) NULL COMMENT "CORREO ELECTRONICO DEL ASESOR",
  `CELULAR_ASESOR_VENTAS` VARCHAR(20) NULL COMMENT "CELULAR DEL ASESOR",
  PRIMARY KEY (`PK_COD_PROVEEDOR`))
COMMENT "TABLA QUE REGISTRA LOS PROVEEDORES [ABR:PROVEEDO]",
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AS`.`AS_COMPRAS`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AS`.`AS_COMPRAS` ;

CREATE TABLE IF NOT EXISTS `AS`.`AS_COMPRAS` (
  `PK_COD_COMPRA` INT NOT NULL AUTO_INCREMENT COMMENT "CODIGO UNICO DE CADA REGISTRO",
  `NO_FACTURA_COMPRA` VARCHAR(30) NULL COMMENT "NUMERO LEGAL DE LA FACTURA",
  `COD_PROVEEDOR` INT NOT NULL COMMENT "CODIGO DEL PROVEEDOR",
  `FEC_COMPRA` DATE NOT NULL COMMENT "FECHA DE LA COMPRA",
  `SUBTOTAL` DECIMAL(7,2) NOT NULL COMMENT "SUBTOTAL DE LA COMPRA",
  `DESCUENTO` DECIMAL(7,2) NULL COMMENT "DESCUENTO OTORGADO DE LA COMPRA",
  `ISV` DECIMAL(7,2) NOT NULL COMMENT "IMPUESTO SOBRE VENTA DE LA COMPRA",
  `TOTAL_PAGAR` DECIMAL(7,2) NOT NULL COMMENT "TOTAL A PAGAR DE LA COMPRA",
  PRIMARY KEY (`PK_COD_COMPRA`),
  INDEX `IDX_COMPRAS_1` (`COD_PROVEEDOR` ASC),
  CONSTRAINT `FK_COMPRAS_PROVEEDO`
    FOREIGN KEY (`COD_PROVEEDOR`)
    REFERENCES `AS`.`AS_PROVEEDORES` (`PK_COD_PROVEEDOR`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
COMMENT "TABLA QUE REGISTRA LAS COMPRAS [ABR: COMPRAS]",
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AS`.`AS_CATEGORIA_MATERIAL`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AS`.`AS_CATEGORIA_MATERIAL` ;

CREATE TABLE IF NOT EXISTS `AS`.`AS_CATEGORIA_MATERIAL` (
  `PK_COD_CATEGORIA` INT NOT NULL AUTO_INCREMENT COMMENT "CODIGO UNICO DE CADA REGISTRO",
  `NOM_CATEGORIA` VARCHAR(45) NOT NULL COMMENT "NOMBRE DE LA CATEGORIA",
  `DET_CATEGORIA` TEXT NULL COMMENT "DETALLE DE LA CATEGORIA",
  PRIMARY KEY (`PK_COD_CATEGORIA`))
COMMENT "TABLA QUE REGISTRA LAS CATEGORIAS DE LOS MATERIALES [ABR:CATEGORIAS]",
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AS`.`AS_INVENTARIO_MATERIALES`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AS`.`AS_INVENTARIO_MATERIALES` ;

CREATE TABLE IF NOT EXISTS `AS`.`AS_INVENTARIO_MATERIALES` (
  `PK_COD_MATERIAL` INT NOT NULL AUTO_INCREMENT COMMENT "CODIGO UNICO DE CADA REGISTRO",
  `COD_BARRAS` VARCHAR(30) NULL COMMENT "CODIGO DE BARRAS DEL MATERIAL",
  `NOM_MATERIAL` VARCHAR(45) NOT NULL COMMENT "NOMBRE DEL MATERIAL",
  `DET_MATERIAL` TEXT NULL COMMENT "DETALLE DEL MATERIAL",
  `COD_CATEGORIA` INT NOT NULL COMMENT "CODIGO DE CATEGORIA",
  `PRECIO_VENTA` DECIMAL(7,2) NOT NULL COMMENT "PRECIO DE VENTA DEL MATERIAL",
  `EXISTENCIAS` DECIMAL(7,2) NOT NULL COMMENT "EXISTENCIAS DEL MATERIAL EN INVENTARIO",
  `MIN_EXISTENCIAS` DECIMAL(7,2) NOT NULL COMMENT "MINIMO DE EXISTENCIAS DE MATERIAL QUE PUEDE HACER EN INVENTARIO",
  `MAX_EXISTENCIAS` DECIMAL(7,2) NOT NULL COMMENT "MAXIMO DE EXISTENCIAS DE MATERIAL QUE PUEDE HACER EN INVENTARIO",
  PRIMARY KEY (`PK_COD_MATERIAL`),
  INDEX `IDX_INVMATERL_1` (`COD_CATEGORIA` ASC),
  CONSTRAINT `FK_INVMATERL_CATEGORIAS`
    FOREIGN KEY (`COD_CATEGORIA`)
    REFERENCES `AS`.`AS_CATEGORIA_MATERIAL` (`PK_COD_CATEGORIA`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
COMMENT "TABLA CON EL REGISTRO DEL INVENTARIO DE MATERIALES [ABR:INVMATERL]",
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AS`.`AS_MATERIAL_COMPRA_PROVEEDOR`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AS`.`AS_MATERIAL_COMPRA_PROVEEDOR` ;

CREATE TABLE IF NOT EXISTS `AS`.`AS_MATERIAL_COMPRA_PROVEEDOR` (
  `PK_COD_MCP` INT NOT NULL AUTO_INCREMENT COMMENT "CODIGO UNICO DE CADA REGISTRO",
  `COD_MATERIAL` INT NOT NULL COMMENT "CODIGO UNICO DE CADA MATERIAL",
  `COD_PROVEEDOR` INT NOT NULL COMMENT "CODIGO UNICO DE CADA PROVEEDOR",
  `PRECIO_COMPRA` DECIMAL(7,2) NOT NULL COMMENT "PRECIO DE COMPRA DEL MATERIAL",
  PRIMARY KEY (`PK_COD_MCP`),
  INDEX `IDX_MTCOMPPROV_1` (`COD_PROVEEDOR` ASC),
  INDEX `IDX_MTCOMPPROV_2` (`COD_MATERIAL` ASC),
  CONSTRAINT `FK_MTCOMPPROV_PROVEEDO`
    FOREIGN KEY (`COD_PROVEEDOR`)
    REFERENCES `AS`.`AS_PROVEEDORES` (`PK_COD_PROVEEDOR`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_MTCOMPPROV_INVMATERL`
    FOREIGN KEY (`COD_MATERIAL`)
    REFERENCES `AS`.`AS_INVENTARIO_MATERIALES` (`PK_COD_MATERIAL`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
COMMENT "TABLA QUE REGISTRA EL PRECIO DE COMPRA DE UN MATERIAL A UN DETERMINADO PROVEEDOR [ABR: MTCOMPPROV]",
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AS`.`AS_COTIZACIONES`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AS`.`AS_COTIZACIONES` ;

CREATE TABLE IF NOT EXISTS `AS`.`AS_COTIZACIONES` (
  `PK_COD_COTIZACION` INT NOT NULL AUTO_INCREMENT COMMENT "CODIGO UNICO DE CADA REGISTRO",
  `COD_CLIENTE` INT NOT NULL COMMENT "CODIGO UNICO DE CADA CLIENTE",
  `ID_USUARIO` INT NOT NULL COMMENT "IDENTIFICADOR UNICO DE CADA USUARIO",
  `FEC_COTIZACION` DATE NOT NULL COMMENT "FECHA DE COTIZACION",
  `HORA_COTIZACION` TIME NOT NULL COMMENT "HORA DE COTIZACION",
  `SUBTOTAL` DECIMAL(7,2) NOT NULL COMMENT "SUBTOTAL DE COTIZACION",
  `DESCUENTO` DECIMAL(7,2) NULL COMMENT "DESCUENTO DE COTIZACION",
  `ISV` DECIMAL(7,2) NOT NULL COMMENT "IMPUESTO SOBRE VENTA DE COTIZACION",
  `TOTAL_PAGAR` DECIMAL(7,2) NOT NULL COMMENT "TOTAL A PAGAR DE COTIZACION",
  PRIMARY KEY (`PK_COD_COTIZACION`),
  INDEX `IDX_COTIZACIONES_1` (`COD_CLIENTE` ASC),
  INDEX `IDX_COTIZACIONES_2` (`ID_USUARIO` ASC),
  CONSTRAINT `FK_COTIZACIONES_CLIENTES`
    FOREIGN KEY (`COD_CLIENTE`)
    REFERENCES `AS`.`AS_CLIENTES` (`PK_COD_CLIENTE`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_COTIZACIONES_USUARIOSSTM`
    FOREIGN KEY (`ID_USUARIO`)
    REFERENCES `AS`.`AS_USUARIOS_SISTEMA` (`PK_ID_USUARIO`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
COMMENT "TABLA QUE REGISTRA TODAS LA COTIZACIONES REALIZADA [ABR: COTIZACIONES]",
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AS`.`AS_DETALLES_COTIZACIONES`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AS`.`AS_DETALLES_COTIZACIONES` ;

CREATE TABLE IF NOT EXISTS `AS`.`AS_DETALLES_COTIZACIONES` (
  `PK_COD_DET_COTIZACION` INT NOT NULL AUTO_INCREMENT COMMENT "CODIGO UNICO DE CADA REGISTRO",
  `COD_COTIZACION` INT NOT NULL COMMENT "CODIGO DE COTIZACION",
  `NOM_PRODUCTO` VARCHAR(45) NOT NULL COMMENT "NOMBRE CORTO DEL PRODUCTO",
  `DET_PRODUCTO` TEXT NULL COMMENT "DETALLE DESCRIPTIVO DEL PRODUCTO",
  `CANT_PRODUCTO` INT NOT NULL COMMENT "CANTIDAD COTIZADA",
  `PRECIO_PRODUCTO` DECIMAL(7,2) NOT NULL COMMENT "PRECIO DEL PRODUCTO",
  `TOTAL_PAGAR_PRODUCTO` DECIMAL(7,2) NOT NULL COMMENT "TOTAL A PAGAR",
  PRIMARY KEY (`PK_COD_DET_COTIZACION`),
  INDEX `IDX_DETCOTIZACN_1` (`COD_COTIZACION` ASC),
  CONSTRAINT `FK_COTIZACIONES_DETCOTIZACN`
    FOREIGN KEY (`COD_COTIZACION`)
    REFERENCES `AS`.`AS_COTIZACIONES` (`PK_COD_COTIZACION`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
COMMENT "TABLA CON TODAS LOS PRODUCTOS DETALLADOS CON CADA COTIZACION [ABR: DETCOTIZACN]",
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AS`.`AS_ORDENES_TRABAJO`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AS`.`AS_ORDENES_TRABAJO` ;

CREATE TABLE IF NOT EXISTS `AS`.`AS_ORDENES_TRABAJO` (
  `PK_COD_ORDEN_TRABAJO` INT NOT NULL,
  `COD_EMPLEADO` INT NULL,
  `COD_COTIZACION` INT NULL,
  `FEC_ENTREGA` DATE NULL,
  `DET_ORDEN_TRABAJO` TEXT NULL,
  `ESTADO` VARCHAR(20) NULL,
  PRIMARY KEY (`PK_COD_ORDEN_TRABAJO`),
  INDEX `IDX_ORDENTRABAJ_1` (`COD_COTIZACION` ASC),
  INDEX `IDX_ORDENTRABAJ_2` (`COD_EMPLEADO` ASC),
  CONSTRAINT `FK_ORDENTRABAJ_COTIZACIONES`
    FOREIGN KEY (`COD_COTIZACION`)
    REFERENCES `AS`.`AS_COTIZACIONES` (`PK_COD_COTIZACION`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_ORDENTRABAJ_EMPL`
    FOREIGN KEY (`COD_EMPLEADO`)
    REFERENCES `AS`.`AS_EMPLEADOS` (`PK_COD_EMPLEADO`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AS`.`AS_ORDENES_DETALLE_MATERIALES`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AS`.`AS_ORDENES_DETALLE_MATERIALES` ;

CREATE TABLE IF NOT EXISTS `AS`.`AS_ORDENES_DETALLE_MATERIALES` (
  `PK_COD_ODM` INT NOT NULL AUTO_INCREMENT,
  `COD_ORDEN_TRABAJO` INT NOT NULL,
  `COD_MATERIAL` INT NOT NULL,
  `CANT_MATERIAL` DECIMAL(7,2) NOT NULL,
  PRIMARY KEY (`PK_COD_ODM`),
  INDEX `IDX_ODM_1` (`COD_ORDEN_TRABAJO` ASC),
  INDEX `IDX_ODM_2` (`COD_MATERIAL` ASC),
  CONSTRAINT `FK_ODM_ORDENTRABAJ`
    FOREIGN KEY (`COD_ORDEN_TRABAJO`)
    REFERENCES `AS`.`AS_ORDENES_TRABAJO` (`PK_COD_ORDEN_TRABAJO`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_ODM_INVMATERL`
    FOREIGN KEY (`COD_MATERIAL`)
    REFERENCES `AS`.`AS_INVENTARIO_MATERIALES` (`PK_COD_MATERIAL`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AS`.`AS_VENTAS`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AS`.`AS_VENTAS` ;

CREATE TABLE IF NOT EXISTS `AS`.`AS_VENTAS` (
  `PK_COD_VENTA` INT NOT NULL AUTO_INCREMENT COMMENT "CODIGO UNICO DE CADA REGISTRO",
  `NO_FACTURA` VARCHAR(45) NULL COMMENT "NUMERO DE FACTURA (SEGUN LA LEY)",
  `COD_COTIZACION` INT NOT NULL COMMENT "CODIGO UNICO ASOCIADO A LA COTIZACION",
  `COD_ORDEN_TRABAJO` INT NOT NULL COMMENT "CODIGO UNICO ASOCIADO A LA ORDEN DE TRABAJO",
  `ID_USUARIO_VENTA` INT NOT NULL COMMENT "IDENTIFICADOR DEL USUARIO QUE REALIZA LA VENTA",
  `FEC_VENTA` DATE NOT NULL COMMENT "FECHA DE LA VENTA",
  `HORA_VENTA` TIME NOT NULL COMMENT "HORA DE LA VENTA",
  `TIPO_PAGO` VARCHAR(20) NOT NULL COMMENT "TIPO DE PAGO (CONTADO, CREDITO)",
  `COD_METODO_PAGO` INT NOT NULL COMMENT "CODIGO UNICO DEL METODO DE PAGO",
  `SUBTOTAL` DECIMAL(7,2) NOT NULL COMMENT "SUBOTOTAL DE LA VENTA",
  `DESCUENTO` DECIMAL(7,2) NULL COMMENT "DESCUENTO OTORGADO",
  `ISV` DECIMAL(7,2) NOT NULL COMMENT "IMPUESTO SOBRE VENTA",
  `TOTAL_PAGAR` DECIMAL(7,2) NOT NULL COMMENT "TOTAL A PAGAR DE LA VENTA",
  PRIMARY KEY (`PK_COD_VENTA`),
  INDEX `IDX_VENTAS_1` (`COD_METODO_PAGO` ASC),
  INDEX `IDX_VENTAS_2` (`ID_USUARIO_VENTA` ASC),
  INDEX `IDX_VENTAS_3` (`COD_ORDEN_TRABAJO` ASC),
  INDEX `IDX_VENTAS_4` (`COD_COTIZACION` ASC),
  CONSTRAINT `FK_VENTAS_METPAGOS`
    FOREIGN KEY (`COD_METODO_PAGO`)
    REFERENCES `AS`.`AS_METODOS_PAGOS` (`PK_COD_METODO_PAGO`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_VENTAS_USUARIOSSTM`
    FOREIGN KEY (`ID_USUARIO_VENTA`)
    REFERENCES `AS`.`AS_USUARIOS_SISTEMA` (`PK_ID_USUARIO`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_VENTAS_ORDENTRABAJ`
    FOREIGN KEY (`COD_ORDEN_TRABAJO`)
    REFERENCES `AS`.`AS_ORDENES_TRABAJO` (`PK_COD_ORDEN_TRABAJO`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_VENTAS_COTIZACIONES`
    FOREIGN KEY (`COD_COTIZACION`)
    REFERENCES `AS`.`AS_COTIZACIONES` (`PK_COD_COTIZACION`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
COMMENT "TABLA QUE REGISTRA LAS VENTAS EN EL SISTEMA [ABR: VENTAS]",
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AS`.`AS_CUENTAS_DETALLE_VENTAS`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AS`.`AS_CUENTAS_DETALLE_VENTAS` ;

CREATE TABLE IF NOT EXISTS `AS`.`AS_CUENTAS_DETALLE_VENTAS` (
  `PK_COD_CDV` INT NOT NULL AUTO_INCREMENT COMMENT "CODIGO UNICO DE CADA REGISTRO DE LA TABLA",
  `NO_CUENTA` INT NOT NULL COMMENT "NUMERO DE CUENTA ASOCIADO",
  `COD_VENTA` INT NOT NULL COMMENT "CODIGO DE VENTA ASOCIADO",
  `FEC_PAGO` DATE NOT NULL COMMENT "FECHA QUE SE NECESITA REALIZAR EL SIGUIENTE PAGO",
  `PERIODICIDAD` VARCHAR(30) NOT NULL COMMENT "PERIODICIDAD DE CADA PAGO (SEMANAL, MENSUAL, TRIMESTRAL, ANUAL)",
  `NO_CUOTAS` INT NOT NULL COMMENT "NUMERO DE CUOTAS",
  PRIMARY KEY (`PK_COD_CDV`),
  INDEX `IDX_CDV_1` (`NO_CUENTA` ASC),
  INDEX `IDX_CDV_2` (`COD_VENTA` ASC),
  CONSTRAINT `FK_CDV_CUENTAS`
    FOREIGN KEY (`NO_CUENTA`)
    REFERENCES `AS`.`AS_CUENTAS_CLIENTES` (`PK_NO_CUENTA`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_CDV_VENTAS`
    FOREIGN KEY (`COD_VENTA`)
    REFERENCES `AS`.`AS_VENTAS` (`PK_COD_VENTA`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
COMMENT "TABLA QUE ASOCIA VENTAS CON UN NUMERO DE CUENTA ESPECIFICO [ABR: CDV]",
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AS`.`AS_SERVICIOS_ENTREGAS`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AS`.`AS_SERVICIOS_ENTREGAS` ;

CREATE TABLE IF NOT EXISTS `AS`.`AS_SERVICIOS_ENTREGAS` (
  `PK_COD_ENTRAGA` INT NOT NULL AUTO_INCREMENT COMMENT "CODIGO UNICO DE CADA REGISTRO DE LA TABLA",
  `COD_VENTAS` INT NOT NULL COMMENT "CODIGO UNICO DE LA TABLA VENTAS ASOCIADO A LA ENTREGA",
  `COD_DIRECCION` INT NOT NULL COMMENT "CODIGO UNICO DE LA TABLA DIRECCIONES CLIENTES ASOCIADO A LA ENTREGA",
  `FEC_ENTREGA` DATE NOT NULL COMMENT "FECHA DE LA ENTREGA",
  `HORA_ENTREGA` TIME NOT NULL COMMENT "HORA DE LA ENTREGA",
  `ESTADO_ENTREGA` TINYINT NOT NULL COMMENT "ESTADO DE LA ENTREGA (EN PROCESO, COMPLETA, ETC.)",
  `OBSERVACIONES` TEXT NULL COMMENT "OBSERVACIONES ADICIONALES",
  PRIMARY KEY (`PK_COD_ENTRAGA`),
  INDEX `IDX_SRENTREGA_1` (`COD_VENTAS` ASC),
  INDEX `IDX_SRENTREGA_2` (`COD_DIRECCION` ASC),
  CONSTRAINT `FK_SRENTREGA_VENTAS`
    FOREIGN KEY (`COD_VENTAS`)
    REFERENCES `AS`.`AS_VENTAS` (`PK_COD_VENTA`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_SRENTREGA_DIRCLN`
    FOREIGN KEY (`COD_DIRECCION`)
    REFERENCES `AS`.`AS_DIRECCIONES_CLIENTES` (`PK_COD_DIRECCION`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
COMMENT "TABLA QUE REGISTRA LAS ENTREGAS PROXIMAS Y LAS QUE YA SE HAN REALIZADO [ABR: SRENTREGA]",
ENGINE = InnoDB;


